﻿using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using System;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            Silnik silnik = new Silnik();
            Zasilacz zasilacz = new Zasilacz();

            kontener.RegisterComponents(silnik, zasilacz);

            silnik.Metoda();

            Console.ReadKey();
        }
    }
}
