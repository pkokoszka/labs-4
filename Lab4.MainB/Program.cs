﻿using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2B;
using System;

namespace Lab4.MainB
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            Silnik silnik = new Silnik();
            ZasilaczB zasilaczb = new ZasilaczB();

            kontener.RegisterComponents(silnik, zasilaczb);

            silnik.Metoda();

            Console.ReadKey();
        }
    }
}
