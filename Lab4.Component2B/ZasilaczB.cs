﻿using ComponentFramework;
using Lab4.Contract;
using System;

namespace Lab4.Component2B
{
    public class ZasilaczB : AbstractComponent, IZasilacz
    {
        public ZasilaczB()
        {
            this.RegisterProvidedInterface<IZasilacz>(this);
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        public void WłączZasilanie()
        {
            Console.WriteLine("Zasilanie wlaczone");
        }

        public void Volty()
        {
            Console.WriteLine("22222");
        }
    }
}
