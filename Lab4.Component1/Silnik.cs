﻿using ComponentFramework;
using Lab4.Contract;
using System;

namespace Lab4.Component1
{
    public class Silnik : AbstractComponent
    {
        IZasilacz zasilacz;

        public Silnik()
        {
            this.RegisterRequiredInterface<IZasilacz>();
        }

        public override void InjectInterface(Type type, object impl)
        {
            zasilacz = (IZasilacz)impl;
        }

        public void Metoda()
        {
            zasilacz.Volty();
        }

        public void Metoda2()
        {
            zasilacz.WłączZasilanie();
        }
    }
}
