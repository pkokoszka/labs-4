﻿using ComponentFramework;
using Lab4.Contract;
using System;

namespace Lab4.Component2
{
    public class Zasilacz : AbstractComponent, IZasilacz
    {
        public Zasilacz()
        {
            this.RegisterProvidedInterface<IZasilacz>(this);
        }       

        public void WłączZasilanie()
        {
            Console.WriteLine("Blad zasilania");
        }

        public void Volty()
        {
            Console.WriteLine("5555");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
